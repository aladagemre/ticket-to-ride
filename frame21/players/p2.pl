%%%%% Changes ( you may delete when you read) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% indent fix (only tabs are used for indent now),
% emre.pl was not drawing a card (plus-minus signs were removed to fix)
% emre.pl draws a random card now
% emre.pl is a semi-human now waiting for a dummy key press at each turn
% gray roads can be filled with a set of SAME colors of any color (decideAction fixed, buildRoad was already working)
% doesnt add to 'hand' anymore if drawed card is 'empty' or 'none'
% note: 'retractall' is a handy method to remove all instances and return true even if there is no predicate
% card drawing is done to get the maximum color we have
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:- module(p2, []).
:- dynamic hand/1. % deck of cards player owns
:- dynamic done/1. % deck of tickets player has connected 
%:- dynamic card_needed/1. % defines if we need to draw more cards because we cant build roads.
:- dynamic card_needed/1. % the color of card we need
:- dynamic histogram/2. % holds the frequency of the colors in current deck.

hand([]).
done([]).
card_needed(true).
%histogram(joker,0).


initialize(PlayerName, PlayerCount) :-
	write('I am '),write(PlayerName),write(' of a '),write(PlayerCount),write(' player game.'),nl,
	hand(Deck), find_color_histogram(Deck),
	getPlayerList(PlayerList),
	length(PlayerList, NumPlayers).
	
decideAction(ActionList, Action) :-
	%(has_unsatisfied_ticket, Action=ticket);
	write('===== deciding ====='), nl,
	hand(Deck), write('deck: '), write(Deck), nl,
	find_color_histogram(Deck), findall(X, histogram(_, X), Hist), write('hist: '), write(Hist), nl,
	max_list(Hist, MaxOfHist), histogram(ColorNeed, MaxOfHist),
	retractall(card_needed(_)), assert(card_needed(ColorNeed)), write('need: '), write(ColorNeed), nl,
	(MaxOfHist<6, Action=card); Action=build.

	%length(Deck, NumberOfCards),
	%histogram(joker, NumberOfJokers),
	%NetNumberOfCards is NumberOfCards-NumberOfJokers,
	%write('Net number of cards:'),write(NetNumberOfCards),nl,
	
	%write('Total Cards:'), write(NumberOfCards),nl,
	%(
	%	card_needed(true)
	%	-> retract(card_needed(true)), asserta(card_needed(false)), Action=card;
	%	
	%	(
	%		NetNumberOfCards<6
	%	->	Action=card;
	%		Action=build
	%	)
	%).
		

drawCard(SlotNo) :-
	card_needed(CardNeed),
	readCardSlot(1, Slot1),
	readCardSlot(2, Slot2),
	readCardSlot(3, Slot3),
	readCardSlot(4, Slot4),
	readCardSlot(5, Slot5),
	((Slot1=CardNeed, SlotNo=1); true),
	((Slot2=CardNeed, SlotNo=2); true),
	((Slot3=CardNeed, SlotNo=3); true),
	((Slot4=CardNeed, SlotNo=4); true),
	((Slot5=CardNeed, SlotNo=5); true).

drawCardResult(Card) :-
	not(Card=empty),
	not(Card=none),
	hand(OldDeck),
	retract(hand(_)),
	NewDeck=[Card|OldDeck],
	assert(hand(NewDeck)),
	find_color_histogram(NewDeck).


selectTickets(TicketList, SelectedTickets) :-
	[_|SelectedTickets]=TicketList.
	
selectTicketResult(SelectedTickets).

%===========BUILDING ROAD =====================	
buildRoad(City1, City2, CardList):- 
	%write('Want to build:'),write(City1),write('--'),write(City2),write(' with '), write(CardList),
	hand(Hdeck),
	find_color_histogram(Hdeck),

	histogram(orange, Count_orange),
	write('# of oranges:'), write(Count_orange),nl,
	histogram(red, Count_red),
	write('# of reds:'), write(Count_red),nl,
	histogram(yellow, Count_yellow),
	write('# of yellows:'), write(Count_yellow),nl,
	histogram(blue, Count_blue),
	write('# of blues:'), write(Count_blue),nl,
	histogram(green, Count_green),
	write('# of greens:'), write(Count_green),nl,
	histogram(purple, Count_purple),
	write('# of purples:'), write(Count_purple),nl,
	histogram(black, Count_black),
	write('# of blacks:'), write(Count_black),nl,
	histogram(white, Count_white),
	write('# of whites:'), write(Count_white),nl,
	histogram(joker, Count_joker),
	write('# of jokers:'), write(Count_joker),nl,
	
	
	% ========= Now we know how many cards of what color we have. ==============
	%==========================
	histogram_more(Color6, 6),
	can_we_connect(City1, City2, 6, Color6), write(City1), write(' - '), write(City2),nl,
	CardList=[Color6, Color6, Color6, Color6, Color6, Color6],
	remove_used_cards(CardList),!;
	
	%==========================
	histogram_more(Color5, 5),
	can_we_connect(City1, City2, 5, Color5), write(City1), write(' - '), write(City2),nl,
	CardList=[Color5, Color5, Color5, Color5, Color5],
	remove_used_cards(CardList),!;
	
	%==========================
	histogram_more(Color4, 4),
	can_we_connect(City1, City2, 4, Color4), write(City1), write(' - '), write(City2),nl,
	CardList=[Color4, Color4, Color4, Color4],
	remove_used_cards(CardList),!;
	
	%==========================
	histogram_more(Color3, 3),
	can_we_connect(City1, City2, 3, Color3), write(City1), write(' - '), write(City2),nl,
	CardList=[Color3, Color3, Color3],
	remove_used_cards(CardList),!;
	
	%==========================
	histogram_more(Color2, 2),
	can_we_connect(City1, City2, 2, Color2), write(City1), write(' - '), write(City2),nl,
	hand(Deck),
	CardList=[Color2, Color2],
	remove_used_cards(CardList),!;
	
	%===========================
	histogram_more(Color1, 1),
	can_we_connect(City1, City2, 1, Color1), write(City1), write(' - '), write(City2),nl, 
	hand(Deck),
	CardList=[Color1],
	remove_used_cards(CardList),!;
	
	
	retract(card_needed(false)),
	asserta(card_needed(true)),!.
	
buildRoadResult(Success) :-
	Success,
	write('Road has been built.'),
	nl
	;
	write('Road could not be built.'),
	nl.

% ============= ANNOUNCE FUNCTIONS ============================

announceAction(drawcard, Player, CardList) :-
	write(Player),write(' received card(s): '),write(CardList),nl.
announceAction(selectticket, Player, TicketCount) :-
	write(Player),write(' received '),write(TicketCount),write(' tickets.'),nl.
announceAction(buildroad, Player, City1, City2, CardList) :-
	write(Player),write(' built route: '),write(City1),write('-'),write(City2),write(' with '),write(CardList),nl.
announceAction(clearfaceup, DiscardedCardList) :-
	write('Face-up cards are discarded: '),write(DiscardedCardList),nl.
announceAction(timeout, Player) :-
	write(Player),write(' run out of time!'),nl.
% =================================================================


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Some Inner Methods %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

color_count(Color, Count, Deck) :-
	findall(X, (member(X, Deck), X=Color), Y),
	length(Y, Count).

find_color_histogram(Deck):-
	color_count(orange, Count1, Deck),
	retractall(histogram(orange, _)),
	assert(histogram(orange, Count1)),

	color_count(red, Count2, Deck),
	retractall(histogram(red, _)),
	assert(histogram(red, Count2)),

	color_count(yellow, Count3, Deck),
	retractall(histogram(yellow, _)),
	assert(histogram(yellow, Count3)),

	color_count(blue, Count4, Deck),
	retractall(histogram(blue, _)),
	assert(histogram(blue, Count4)),

	color_count(green, Count5, Deck),
	retractall(histogram(green, _)),
	assert(histogram(green, Count5)),

	color_count(purple, Count6, Deck),
	retractall(histogram(purple, _)),
	assert(histogram(purple, Count6)),

	color_count(black, Count7, Deck),
	retractall(histogram(black, _)),
	assert(histogram(black, Count7)),

	color_count(white, Count8, Deck),
	retractall(histogram(white, _)),
	assert(histogram(white, Count8)),

	color_count(joker, Count9, Deck),
	retractall(histogram(joker, _)),
	assert(histogram(joker, Count9)).

%======== LIST REMOVAL FUNCTIONS ==============
subtract_once(List, [], List).
subtract_once(List, [Item|Delete], Result):-
	(select(Item, List, NList)->
	subtract_once(NList, Delete, Result);
	(List\=[],subtract_once(List, Delete, Result))).
	
remove_used_cards(CardList):-
	hand(Deck),
	subtract_once(Deck, CardList, NewDeck),
	retract(hand(_)),
	asserta(hand(NewDeck)),!.
	
histogram_more(Color, MinAmount):-
	histogram(Color, X),
	X>=MinAmount.

can_we_connect(City1, City2, Len, Color):-
	connected(City1, City2, Len, Color, _, empty),!;
	connected(City1, City2, Len, gray, _, empty),!.
	
%==============================================

%%% TODO %%%%%
% 1. Which card to draw?
% 2. Will there be 2 players or more? What about double-roads? Do we have to prevent it?
% 3. We have to find a way to use jokers. Now we are not intelligent enough to use it.
% 4. When does the game finish? When we get card 'empty'. But how to stop the program? Frame doesn't stop.
% 5. How to interpret tickets. How to choose the best path to fill according to these tickets.
%%%%%%%%%%%%
