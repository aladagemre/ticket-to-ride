?-consult(map).
?-consult(tickets).
?-consult(deck).
?-use_module(library(random)).

:- dynamic current_player/1.



next_player(Current, New) :- 
	num_users(N),
	(
		Current < N,
		New is Current+1,!
	);
	(
		New = 1
	).
	
give_cards_to_player(N, Player):-
	writef("Giving %d cards to %w\n", [N, Player]),
	draw_n_cards_from_deck(N, [],  Cards),
	player_cards(Player, OldCards),
	append(OldCards, Cards, NewCards),
	retract(player_cards(Player,_)),
	asserta(player_cards(Player, NewCards)).

map(FunctionName,[H|T],[NH|NT]):-
   Function=..[FunctionName,H,NH],
   call(Function),
   map(FunctionName,T,NT).
map(_,[],[]).

suggest_3_tickets_to_player(MustTake, Player):-
	writef("Suggesting 3 tickets to %w\n", [Player]),
	writef("Must take at least %d of these tickets.\n", [MustTake]),
	draw_n_tickets(3, [],  Tickets),
	map(print_ticket, Tickets, L),
	player_tickets(Player, OldTickets),
	write('Enter the tickets youd like to keep\n'),
	read(X),
	%LengthOfKept is length(TicketsKept),
	%LengthOfKept >= MustTake,
	append(OldTickets, TicketsKept, NewTickets),
	retract(player_tickets(Player,_)),
	asserta(player_tickets(Player, NewTickets)).
	
% ====== SETTING UP THE GAME ========
ask_number_of_users :- write('How many users will there be?'), read(Num_user), asserta(num_users(Num_user)).
current_player(1).
main_loop :-
	suggest_3_tickets_to_player(2, player1),
	suggest_3_tickets_to_player(2, player2),
	suggest_3_tickets_to_player(2, player3),
	suggest_3_tickets_to_player(2, player4),
	repeat,	
    current_player(I),
    writef('Its %d. players turn: ', [I]),   
    read(Move),
    writeln(Move),
	next_player(I, New),
	retract(current_player(Old)),
	asserta(current_player(New)),
	fail.

?-ask_number_of_users.
?-main_loop.