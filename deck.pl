:- dynamic deck/2.
:- dynamic last_card/1.
:- dynamic player_cards/2.

player_cards(player1, []).
player_cards(player2, []).
player_cards(player3, []).
player_cards(player4, []).
player_cards(player5, []).
player_cards(player6, []).

deck(0, purple).
deck(1, purple).
deck(2, purple).
deck(3, purple).
deck(4, purple).
deck(5, purple).
deck(6, purple).
deck(7, purple).
deck(8, purple).
deck(9, purple).
deck(10, purple).
deck(11, purple).
deck(12, white).
deck(13, white).
deck(14, white).
deck(15, white).
deck(16, white).
deck(17, white).
deck(18, white).
deck(19, white).
deck(20, white).
deck(21, white).
deck(22, white).
deck(23, white).
deck(24, blue).
deck(25, blue).
deck(26, blue).
deck(27, blue).
deck(28, blue).
deck(29, blue).
deck(30, blue).
deck(31, blue).
deck(32, blue).
deck(33, blue).
deck(34, blue).
deck(35, blue).
deck(36, yellow).
deck(37, yellow).
deck(38, yellow).
deck(39, yellow).
deck(40, yellow).
deck(41, yellow).
deck(42, yellow).
deck(43, yellow).
deck(44, yellow).
deck(45, yellow).
deck(46, yellow).
deck(47, yellow).
deck(48, brown).
deck(49, brown).
deck(50, brown).
deck(51, brown).
deck(52, brown).
deck(53, brown).
deck(54, brown).
deck(55, brown).
deck(56, brown).
deck(57, brown).
deck(58, brown).
deck(59, brown).
deck(60, black).
deck(61, black).
deck(62, black).
deck(63, black).
deck(64, black).
deck(65, black).
deck(66, black).
deck(67, black).
deck(68, black).
deck(69, black).
deck(70, black).
deck(71, black).
deck(72, red).
deck(73, red).
deck(74, red).
deck(75, red).
deck(76, red).
deck(77, red).
deck(78, red).
deck(79, red).
deck(80, red).
deck(81, red).
deck(82, red).
deck(83, red).
deck(84, green).
deck(85, green).
deck(86, green).
deck(87, green).
deck(88, green).
deck(89, green).
deck(90, green).
deck(91, green).
deck(92, green).
deck(93, green).
deck(94, green).
deck(95, green).
deck(96, locomotive).
deck(97, locomotive).
deck(98, locomotive).
deck(99, locomotive).
deck(100, locomotive).
deck(101, locomotive).
deck(102, locomotive).
deck(103, locomotive).
deck(104, locomotive).
deck(105, locomotive).
deck(106, locomotive).
deck(107, locomotive).
deck(108, locomotive).
deck(109, locomotive).


deck(['purple', 'purple', 'purple', 'purple', 'purple', 'purple', 'purple', 'purple', 'purple', 'purple', 'purple', 'purple', 'white', 'white', 'white', 'white', 'white', 'white', 'white', 'white', 'white', 'white', 'white', 'white', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'blue', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'yellow', 'brown', 'brown', 'brown', 'brown', 'brown', 'brown', 'brown', 'brown', 'brown', 'brown', 'brown', 'brown', 'black', 'black', 'black', 'black', 'black', 'black', 'black', 'black', 'black', 'black', 'black', 'black', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'red', 'green', 'green', 'green', 'green', 'green', 'green', 'green', 'green', 'green', 'green', 'green', 'green', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive', 'locomotive']).


% ====== SOME TOOLS ========
for_card(I, I, N) :- I =< N.
for_card(I, M, N) :-
	M < N,
	M1 is M + 1,
	for_card(I, M1, N).
for_card(I, N) :- for_card(I, 1, N).


swap_card(I, J):-
	I=J,!.
swap_card(I,J):-
	retract(deck(I, A)),
	retract(deck(J, B)),
	asserta(deck(I, B)),
	asserta(deck(J, A)),
	!.
	
shuffle_deck :-
	for_card(I, 109),
	shuffle_deck(I),
	fail.
shuffle_deck :- true.
shuffle_deck(I) :-
	 J is random( 55)+1,
	swap_card(I, J),
	!.


?-shuffle_deck.
last_card(109).
draw_n_cards_from_deck(0, Existing_cards, Existing_cards):- !.
draw_n_cards_from_deck(N, Existing_cards, Drawn_cards):- 
%	N>=1,!,
	last_card(Last),
	Last>=1,
	retract(last_card(Last)),
	NewLast is Last - 1,
	asserta(last_card(NewLast)),
	K is N - 1,
	draw_n_cards_from_deck(K, Existing_cards, New_cards),
	deck(Last, LastCard),
	append(New_cards, [LastCard], Drawn_cards).
