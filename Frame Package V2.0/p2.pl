:- module(p2, []).
:- dynamic hand/1. % deck of cards player owns
:- dynamic done/1. % deck of tickets player has connected 

initialize(PlayerName, PlayerCount) :-
	write('I am '),write(PlayerName),write(' of a '),write(PlayerCount),write(' player game.'),nl,
    assert(hand([])),
    assert(done([])).
	
decideAction(ActionList, Action) :-
    %(has_unsatisfied_ticket, Action=ticket);
    hand(Deck), write('===== cards: '), write(Deck), write(' ====='), nl,
    color_count(orange, Count, Deck), write('===== oranges: '), write(Count), write(' ====='), nl,
    Action=card.

drawCard(SlotNo) :-
    SlotNo=0.
drawCardResult(Card) :-
    hand(OldDeck),
    retract(hand(_)),
    NewDeck=[Card|OldDeck],
    assert(hand(NewDeck)).

selectTickets(TicketList, SelectedTickets) :-
    [_|SelectedTickets]=TicketList.
selectTicketResult(SelectedTickets).

buildRoad(City1, City2, CardList).
buildRoadResult(Success) :-
	Success,
	write('Road has been built.'),
	nl
	;
	write('Road could not be built.'),
	nl.


announceAction(drawcard, Player, CardList) :-
    write(Player),write(' received card(s): '),write(CardList),nl.
announceAction(selectticket, Player, TicketCount) :-
    write(Player),write(' received '),write(TicketCount),write(' tickets.'),nl.
announceAction(buildroad, Player, City1, City2, CardList) :-
    write(Player),write(' built route: '),write(City1),write('-'),write(City2),write(' with '),write(CardList),nl.
announceAction(clearfaceup, DiscardedCardList) :-
    write('Face-up cards are discarded: '),write(DiscardedCardList),nl.
announceAction(timeout, Player) :-
    write(Player),write(' run out of time!'),nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Some Inner Methods %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

color_count(Color, Count, Deck) :-
    findall(X, (member(X, Deck), X=Color), Y),
    length(Y, Count).

