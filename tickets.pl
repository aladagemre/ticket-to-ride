:- dynamic ticket/4.
:- dynamic last_ticket/1.
:- dynamic player_tickets/2.

player_tickets(player1, []).
player_tickets(player2, []).
player_tickets(player3, []).
player_tickets(player4, []).
player_tickets(player5, []).
player_tickets(player6, []).

ticket(0, los_angeles, chicago, 16).
ticket(1, helena, los_angeles, 8).
ticket(2, kansas_city, houston, 5).
ticket(3, seattle, new_york, 20).
ticket(4, chicago, santa_fe, 9).
ticket(5, boston, miami, 12).
ticket(6, toronto, miami, 10).
ticket(7, chicago, new_orleans, 7).
ticket(8, dallas, new_york, 11).
ticket(9, los_angeles, new_york, 20).
ticket(10, los_angeles, miami, 19).
ticket(11, seattle, los_angeles, 9).
ticket(12, duluth, houston, 8).
ticket(13, winnipeg, houston, 12).
ticket(14, new_york, atlanta, 6).
ticket(15, denver, pittsburgh, 11).
ticket(16, sault_st_marie, nashville, 8).
ticket(17, montreal, new_orleans, 13).
ticket(18, vancouver, montreal, 20).
ticket(19, vancouver, santa_fe, 13).
ticket(20, denver, el_paso, 4).
ticket(21, calgary, salt_lake_city, 7).
ticket(22, portland, nashville, 17).
ticket(23, duluth, el_paso, 10).
ticket(24, portland, phoenix, 11).
ticket(25, sault_st_marie, oklahoma_city, 8).
ticket(26, winnipeg, little_rock, 11).
ticket(27, san_fransisco, atlanta, 17).
ticket(28, montreal, atlanta, 9).
ticket(29, washington, atlanta, 4).
ticket(30, pittsburgh, new_orleans, 8).
ticket(31, vancouver, denver, 11).
ticket(32, calgary, nashville, 14).
ticket(33, kansas_city, boston, 11).
ticket(34, toronto, charleston, 6).
ticket(35, san_fransisco, sault_st_marie, 17).
ticket(36, denver, saint_louis, 6).
ticket(37, portland, pittsburgh, 19).
ticket(38, san_fransisco, washington, 21).
ticket(39, omaha, new_orleans, 8).
ticket(40, winnipeg, santa_fe, 10).
ticket(41, phoenix, boston, 19).
ticket(42, salt_lake_city, kansas_city, 7).


% ====== SOME TOOLS ========
for(I, I, N) :- I =< N.
for(I, M, N) :-
	M < N,
	M1 is M + 1,
	for(I, M1, N).
for(I, N) :- for(I, 1, N).


swap(I, J):-
	I=J,!.
swap(I,J):-
	retract(ticket(I, A, B, C)),
	retract(ticket(J, D, E, F)),
	asserta(ticket(I, D, E, F)),
	asserta(ticket(J, A, B, C)),
	!.
	
shuffle :-
	for(I, 42),
	shuffle(I),
	fail.
shuffle :- true.
shuffle(I) :-
	 J is random( 21)+1,
	swap(I, J),
	!.


?-shuffle.
last_ticket(42).
draw_n_tickets(0, Existing_tickets, Existing_tickets):- !.
draw_n_tickets(N, Existing_tickets, Drawn_tickets):- 
%	N>=1,!,
	last_ticket(Last),
	Last>=1,
	retract(last_ticket(Last)),
	NewLast is Last - 1,
	asserta(last_ticket(NewLast)),
	K is N - 1,
	draw_n_tickets(K, Existing_tickets, New_tickets),
	append(New_tickets, [Last], Drawn_tickets).

print_ticket(Ticket, L):-
	ticket(Ticket, From, To, Points),
	writef("%d) %w - %w (%d)\n", [Ticket, From, To, Points]),
	L = [Ticket, From, To, Points].


print_tickets(Tickets):-
	H|T = Tickets,	
	print_ticket(H),
	print_tickets(T).
	
	
    split(String, "", Result) :- !,
        characters_to_strings(String, Result).
    split(String, Delimiters, Result) :-
        real_split(String, Delimiters, Result).

    characters_to_strings([], []).
    characters_to_strings([C|Cs], [[C]|Ss]) :-
        characters_to_strings(Cs, Ss).

    real_split(String, Delimiters, Result) :-
        (   append(Substring, [Delimiter|Rest], String),
            memberchk(Delimiter, Delimiters)
        ->  Result = [Substring|Substrings],
            real_split(Rest, Delimiters, Substrings)
        ;   Result = [String]
        ). 