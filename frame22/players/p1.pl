:- module(p1, []).
:- dynamic hand/1.		% deck of cards player owns
:- dynamic card_needed/1.	% color of card player needs
:- dynamic histogram/2.		% holds the frequency of the colors in current deck.
:- dynamic joker_number/1.	% number of jokers owned
:- dynamic card_owned/1.	% check the number of wagons (45 max)

hand([]).
card_owned(0).

initialize(PlayerName, PlayerCount) :-
	write('I am '),write(PlayerName),write(' of a '),write(PlayerCount),write(' player game.'),nl,
	hand(Deck), find_color_histogram(Deck),
	getPlayerList(PlayerList),
	length(PlayerList, NumPlayers).

decideAction(ActionList, Action) :-

	% ===== analyse the state =====
	hand(Deck),
	%write('deck: '), write(Deck), nl,
	find_color_histogram(Deck), findall(X, histogram(_, X), Hist),
	%write('hist: '), write(Hist), nl,
	joker_number(JokerNo),
	%write('joker: '), write(JokerNo), nl,
	NetNo=MaxOfHist+JokerNo,
	max_list(Hist, MaxOfHist), findall(X, histogram(X, MaxOfHist), ColorNeed),
	%write('need: '), write(ColorNeed), nl,
	retractall(card_needed(_)), assert(card_needed(ColorNeed)),
	card_owned(CardOwn),
	%write('owned: '), write(CardOwn), nl,
	% =============================

	(CardOwn>44	% if		if we have more cards than we can use 
	-> Action=build	% then		stop drawing cards
	;(NetNo<6	% elseif	if we can't build a road of length 6
	-> Action=card	% then		try to get a set of length 6 by drawing cards
	; Action=build	% else		otherwise build the road of length 6
	)).

drawCard(SlotNo) :-
	% card needed is the color of cards we have with maximum numbers
	% check the open cards to see if there's anything we need
	% draw a closed card otherwise (and never a joker not to waste a turn)
	card_needed(CardNeed),
	readCardSlot(1, Slot1),
	readCardSlot(2, Slot2),
	readCardSlot(3, Slot3),
	readCardSlot(4, Slot4),
	readCardSlot(5, Slot5),
	((member(Slot1, CardNeed), SlotNo=1); true),
	((member(Slot2, CardNeed), SlotNo=2); true),
	((member(Slot3, CardNeed), SlotNo=3); true),
	((member(Slot4, CardNeed), SlotNo=4); true),
	((member(Slot5, CardNeed), SlotNo=5); true).

drawCardResult(Card) :-
	% save the drawed card to the hand
	not(Card=empty),
	not(Card=none),
	hand(OldDeck),
	retract(hand(_)),
	NewDeck=[Card|OldDeck],
	assert(hand(NewDeck)),
	card_owned(OldCount),
	retractall(card_owned(_)),
	NewCount is OldCount+1,
	assert(card_owned(NewCount)),
	find_color_histogram(NewDeck).

selectTickets(TicketList, SelectedTickets) :-
	% select 2 tickets of minimum costs to minimize loss
	findall(X, (member(N, TicketList), ticket(N, _, _, X)), Points),
	max_list(Points, MaxPoint),
	findall(N, (member(N, TicketList), ticket(N, _, _, MaxPoint)), Maximums),
	[Maximum|_]=Maximums,
	select(Maximum, TicketList, SelectedTickets).
	%[_|SelectedTickets]=TicketList.
	
selectTicketResult(SelectedTickets).

%===========BUILDING ROAD =====================	
buildRoad(City1, City2, CardList):- 
	%write('Want to build:'),write(City1),write('--'),write(City2),write(' with '), write(CardList),
	hand(Hdeck),
	find_color_histogram(Hdeck),

	%histogram(orange, Count_orange),
	%write('# of oranges:'), write(Count_orange),nl,
	%histogram(red, Count_red),
	%write('# of reds:'), write(Count_red),nl,
	%histogram(yellow, Count_yellow),
	%write('# of yellows:'), write(Count_yellow),nl,
	%histogram(blue, Count_blue),
	%write('# of blues:'), write(Count_blue),nl,
	%histogram(green, Count_green),
	%write('# of greens:'), write(Count_green),nl,
	%histogram(purple, Count_purple),
	%write('# of purples:'), write(Count_purple),nl,
	%histogram(black, Count_black),
	%write('# of blacks:'), write(Count_black),nl,
	%histogram(white, Count_white),
	%write('# of whites:'), write(Count_white),nl,
	%joker_number(Count_joker),
	%write('# of jokers:'), write(Count_joker),nl,
	
	
	% Try to build the road with the maximum length we can
	% ========== length:6 ==========
	joker_number(Count_joker),
	ColorNo6 is 6-Count_joker,
	histogram_more(Color6, ColorNo6),
	can_we_connect(City1, City2, 6, Color6), %write(City1), write(' - '), write(City2),nl,
	%CardList=[Color6, Color6, Color6, Color6, Color6, Color6],
	create_card_list(Color6, ColorNo6, Count_joker, CardList),
	remove_used_cards(CardList),!;
	% ==============================
	
	% ========== length:5 ==========
	joker_number(Count_joker),
	ColorNo5 is 5-Count_joker,
	histogram_more(Color5, ColorNo5),
	can_we_connect(City1, City2, 5, Color5), %write(City1), write(' - '), write(City2),nl,
	%CardList=[Color5, Color5, Color5, Color5, Color5],
	create_card_list(Color5, ColorNo5, Count_joker, CardList),
	remove_used_cards(CardList),!;
	% ==============================
	
	% ========== length:4 ==========
	joker_number(Count_joker),
	ColorNo4 is 4-Count_joker,
	histogram_more(Color4, ColorNo4),
	can_we_connect(City1, City2, 4, Color4), %write(City1), write(' - '), write(City2),nl,
	%CardList=[Color4, Color4, Color4, Color4],
	create_card_list(Color4, ColorNo4, Count_joker, CardList),
	remove_used_cards(CardList),!;
	% ==============================
	
	% ========== length:3 ==========
	joker_number(Count_joker),
	ColorNo3 is 3-Count_joker,
	histogram_more(Color3, ColorNo3),
	can_we_connect(City1, City2, 3, Color3), %write(City1), write(' - '), write(City2),nl,
	%CardList=[Color3, Color3, Color3],
	create_card_list(Color3, ColorNo3, Count_joker, CardList),
	remove_used_cards(CardList),!;
	% ==============================
	
	% ========== length:2 ==========
	joker_number(Count_joker),
	ColorNo2 is 2-Count_joker,
	histogram_more(Color2, ColorNo2),
	can_we_connect(City1, City2, 2, Color2), %write(City1), write(' - '), write(City2),nl,
	hand(Deck),
	%CardList=[Color2, Color2],
	create_card_list(Color2, ColorNo2, Count_joker, CardList),
	remove_used_cards(CardList),!;
	% ==============================
	
	% ========== length:1 ==========
	joker_number(Count_joker),
	ColorNo1 is 1-Count_joker,
	histogram_more(Color1, ColorNo1),
	can_we_connect(City1, City2, 1, Color1), %write(City1), write(' - '), write(City2),nl, 
	hand(Deck),
	%CardList=[Color1],
	create_card_list(Color1, ColorNo1, Count_joker, CardList),
	remove_used_cards(CardList),!.
	% ==============================

buildRoadResult(Success) :-
	Success,
	write('Road has been built.'),
	nl
	;
	write('Road could not be built.'),
	nl.

% ===================================================================
% ============= ANNOUNCE FUNCTIONS ==================================
% ===================================================================
announceAction(drawcard, Player, CardList) :-
	write(Player),write(' received card(s): '),write(CardList),nl.
announceAction(selectticket, Player, TicketCount) :-
	write(Player),write(' received '),write(TicketCount),write(' tickets.'),nl.
announceAction(buildroad, Player, City1, City2, CardList) :-
	write(Player),write(' built route: '),write(City1),write('-'),write(City2),write(' with '),write(CardList),nl.
announceAction(clearfaceup, DiscardedCardList) :-
	write('Face-up cards are discarded: '),write(DiscardedCardList),nl.
announceAction(timeout, Player) :-
	write(Player),write(' run out of time!'),nl.
announceAction(deckshuffled) :-
	write('deckshuffled'), nl.

% ================================================
% ===== INNER FUNCTIONS ==========================
% ================================================
color_count(Color, Count, Deck) :-
	findall(X, (member(X, Deck), X=Color), Y),
	length(Y, Count).

find_color_histogram(Deck):-
	color_count(orange, Count1, Deck),
	retractall(histogram(orange, _)),
	assert(histogram(orange, Count1)),

	color_count(red, Count2, Deck),
	retractall(histogram(red, _)),
	assert(histogram(red, Count2)),

	color_count(yellow, Count3, Deck),
	retractall(histogram(yellow, _)),
	assert(histogram(yellow, Count3)),

	color_count(blue, Count4, Deck),
	retractall(histogram(blue, _)),
	assert(histogram(blue, Count4)),

	color_count(green, Count5, Deck),
	retractall(histogram(green, _)),
	assert(histogram(green, Count5)),

	color_count(purple, Count6, Deck),
	retractall(histogram(purple, _)),
	assert(histogram(purple, Count6)),

	color_count(black, Count7, Deck),
	retractall(histogram(black, _)),
	assert(histogram(black, Count7)),

	color_count(white, Count8, Deck),
	retractall(histogram(white, _)),
	assert(histogram(white, Count8)),

	color_count(joker, Count9, Deck),
	retractall(joker_number(_)),
	assert(joker_number(Count9)).

% ===============================================
% ======== LIST REMOVAL FUNCTIONS ===============
% ===============================================
subtract_once(List, [], List).
subtract_once(List, [Item|Delete], Result):-
	(select(Item, List, NList)->
	subtract_once(NList, Delete, Result);
	(List\=[],subtract_once(List, Delete, Result))).
	
remove_used_cards(CardList):-
	hand(Deck),
	subtract_once(Deck, CardList, NewDeck),
	retract(hand(_)),
	asserta(hand(NewDeck)),!.
	
histogram_more(Color, MinAmount):-
	histogram(Color, X),
	X>=MinAmount.

can_we_connect(City1, City2, Len, Color):-
	connected(City1, City2, Len, Color, _, empty),!;
	connected(City1, City2, Len, gray, _, empty),!.

create_card_list(Color, ColorNo, JokerNo, CardList) :-
	((JokerNo=0, JokerList=[]); true),
	((JokerNo=1, JokerList=[joker]); true),
	((JokerNo=2, JokerList=[joker, joker]); true),
	((JokerNo=3, JokerList=[joker, joker, joker]); true),
	((JokerNo=4, JokerList=[joker, joker, joker, joker]); true),
	((JokerNo=5, JokerList=[joker, joker, joker, joker, joker]); true),
	((JokerNo=6, JokerList=[joker, joker, joker, joker, joker, joker]); true),
	((ColorNo=0, ColorList=[]); true),
	((ColorNo=1, ColorList=[Color]); true),
	((ColorNo=2, ColorList=[Color, Color]); true),
	((ColorNo=3, ColorList=[Color, Color, Color]); true),
	((ColorNo=4, ColorList=[Color, Color, Color, Color]); true),
	((ColorNo=5, ColorList=[Color, Color, Color, Color, Color]); true),
	((ColorNo=6, ColorList=[Color, Color, Color, Color, Color, Color]); true),
	append(JokerList, ColorList, CardList).

