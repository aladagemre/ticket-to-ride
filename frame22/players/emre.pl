:- module(emre, []).

initialize(PlayerName, PlayerCount) :-
	write('I am '),write(PlayerName),write(' of a '),write(PlayerCount),write(' player game.'),nl.
	
decideAction(ActionList, Action) :-
	get_single_char(R), 	% comment: non-stop gameplay
				% uncomment: type a key between turns
	Action=card.
	%connected(City1, City2, 1, gray, Coordinates, empty),
	%drawCardResult(Card),
	%Action=build
	%buildRoad(City1,City2,[Card]).
	

drawCard(SlotNo) :-
	random(0, 6, R),
	SlotNo=R.
drawCardResult(Card).

selectTickets(TicketList, SelectedTickets) :- [_|SelectedTickets]=TicketList.
selectTicketResult(SelectedTickets).

buildRoad(City1, City2, CardList).
buildRoadResult(Success) :-
	Success,
	write('Road has been built.'),
	nl
	;
	write('Road could not be built.'),
	nl.


announceAction(drawcard, Player, CardList) :- write(Player),write(' received card(s): '),write(CardList),nl.
announceAction(selectticket, Player, TicketCount) :- write(Player),write(' received '),write(TicketCount),write(' tickets.'),nl.
announceAction(buildroad, Player, City1, City2, CardList) :- write(Player),write(' built route: '),write(City1),write('-'),write(City2),write(' with '),write(CardList),nl.
announceAction(clearfaceup, DiscardedCardList) :- write('Face-up cards are discarded: '),write(DiscardedCardList),nl.
announceAction(timeout, Player) :- write(Player),write(' run out of time!'),nl.
announceAction(deckshuffled) :- write('deckshuffled'), nl.
